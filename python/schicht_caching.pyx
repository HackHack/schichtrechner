# linux version (compiler directives for cython compiler)
# distutils: language = c++
# distutils: sources = thin_film_calc.cpp
# distutils: extra_compile_args = -fopenmp -ffast-math -O3
# distutils: extra_link_args = -fopenmp
# cython: language_level = 3

# import numpy 
import numpy as np

from libc.stdint cimport int64_t


cdef extern from "thin_film_calc.h":
	void thin_film_calculator  (double *  wavelength,
		                        double *  vec_brechzahl,
		                        double *  vec_absorb,
		                        double *  dn_thickness,
		                        double *  vec_targets,
		                        double *  final_solution,
		                        int num_stuetz ,
		                        int num_schicht,
		                        int num_dick,
		                        int num_mat,
		                        int zwl,
		                        int64_t max_num_iterations,
		                        int size_L1_cache_in_KB)



# define python versions

def _thin_film_calc(double[:] wavelength, 
					double[:] vec_brechzahl, 
					double[:] vec_absorb,
					double[:] dn_thickness, 
					double[:] vec_targets, 
					double[:] final_solution,  
					int64_t num_layers,   
					int64_t num_dick,
				    int64_t num_mat,  
					int64_t zwl, 
					int64_t max_num_iterations,
					int64_t size_L1_cache_in_KB):
	thin_film_calculator (&wavelength[0],  
						  &vec_brechzahl[0], 
						  &vec_absorb[0],
 						  &dn_thickness[0],
						  &vec_targets[0], 
						  &final_solution[0], 
		                  wavelength.shape[0],
		                  num_layers,
		                  num_dick,
		                  num_mat,
		                  zwl,
		                  max_num_iterations,
		                  size_L1_cache_in_KB)





