#include <iostream>
#include <iomanip>
#include <complex>
#include <cmath>
#include <omp.h>
#include <vector>
#include <random>

#include "values_spline.h"  // for spline interpolation
#include "aligned_vector.h"



using namespace std;

// use 64 bit aligned data
template <class T>
using aligned_vector = std::vector<T, alligned_allocator<T, 64>>;




// ###################################################################################################
// helper functions to create input data for test cases - not been optimized !!
// ###################################################################################################



void calc_k_ix_omega_c0 (complex<double> * __restrict__ input_vec , const int num_schicht , const double omega_c0 , complex<double> * __restrict__  output_vec  ) {
    for (int m_i = 0 ; m_i < num_schicht; ++m_i) {
        output_vec[m_i] =  input_vec[m_i] * omega_c0;
    }
}


aligned_vector<double> linespace_wavelength(int num_steps, int lower, int upper)
{
    aligned_vector<double> result(num_steps);
    int rest = upper - lower;
    int part = rest / num_steps;
    for (int i =0 ; i<num_steps; ++i){
        result[i] = lower;
        lower += part;
    }
    return result;
}


void build_data (double * __restrict__ wavelength,
                 double * __restrict__ vec_brechzahl,
                 double * __restrict__ vec_absorb,
                 const int num_stuetz,
                 const int num_mat
                 ){


    // load spline_interpolation from values_spline.h
    interpol AIR = interpol_AIR();
    interpol Substr = interpol_SiO2();
    interpol SiO2 = interpol_SiO2();
    interpol Ta2O5 = interpol_Ta2O5();

    double brech_AIR;
    double absorb_AIR;
    double brech_Si;
    double absorb_Si;
    double brech_Ta;
    double absorb_Ta;
    double brech_Glas;
    double absorb_Glas;

    for (int i = 0; i < num_stuetz; ++i) {
        // values for air = the "medium"
        brech_AIR  = AIR.brech_spline(wavelength[i]);
        absorb_AIR = AIR.absorb_spline(wavelength[i]);
        vec_brechzahl[i*(num_mat+2)] = brech_AIR;
        vec_absorb[i*(num_mat+2)]    = absorb_AIR;

        // values for TiO2  and  SiO2
        brech_Ta  = Ta2O5.brech_spline(wavelength[i]);
        absorb_Ta = Ta2O5.absorb_spline(wavelength[i]);
        brech_Si  = SiO2.brech_spline(wavelength[i]);
        absorb_Si = SiO2.absorb_spline(wavelength[i]);
        vec_brechzahl[i*(num_mat +2) +  1 ] = brech_Ta;
        vec_absorb[i*(num_mat+2) + 1 ]     = absorb_Ta;
        vec_brechzahl[i*(num_mat + 2) +  2]  = brech_Si;
        vec_absorb[i*(num_mat+2) + 2]      = absorb_Si;

        // values for the glas (here again SiO2)
        brech_Glas  = Substr.brech_spline(wavelength[i]);
        absorb_Glas = Substr.absorb_spline(wavelength[i]);
        vec_brechzahl[i*(num_mat+2) +3] = brech_Glas;
        vec_absorb[i*(num_mat+2) +3]    = absorb_Glas;
    }
}






//######################################################################
//#### functions for the "real" computation
//######################################################################


void matrix_product (complex<double>  * __restrict__ A, const complex<double> * __restrict__ B){
	auto temp0 = A[0] * B[0] + A[1] * B[2];
    auto temp1 = A[0] * B[1] + A[1] * B[3];
	auto temp2 = A[2] * B[0] + A[3] * B[2];
	auto temp3 = A[2] * B[1] + A[3] * B[3];
	A[0] = temp0;
	A[1] = temp1;
	A[2] = temp2;
	A[3] = temp3;
}


void calc_interims(const std::complex<double> * __restrict__ k_ix_omega_c0 ,
                   const double * __restrict__ dn_thickness,
                   const int num_mat,
                   const int num_stuetz,
                   const int num_dick,
                   std::complex<double> * __restrict__ precomp_matricies) {

    // allocate some memory
    complex<double> temp1;
    complex<double> temp2;
    complex<double> temp3;
    complex<double> exp_iz;
    complex<double> exp_minus_iz;
    uint64_t pos;

    // define constants
    complex<double> imag_unit{0, 1};
    const std::complex<double> two{2, 0};

#pragma omp simd aligned( k_ix_omega_c0, dn_thickness : 64)
    for (int stuetz_id = 0; stuetz_id < num_stuetz; stuetz_id++) {  // for every wavelength
        for (int dick_id = 0; dick_id < num_dick; dick_id++) {  // for every thickness
            for (int mat_id = 0; mat_id < num_mat; mat_id++) {   // for every material

                temp1 = k_ix_omega_c0[(stuetz_id * num_mat) + mat_id] * dn_thickness[dick_id] * imag_unit;
                // compute complex sine and cosine with the complex exponential function (30% faster)
                // see: http://www.stumblingrobot.com/2016/02/27/prove-some-properties-of-the-complex-sine-and-cosine-functions/
                exp_iz = exp(temp1);
                exp_minus_iz = exp(-temp1);
                temp2 = (exp_iz - exp_minus_iz) / (two * imag_unit);   // = sin(temp1)
                temp3 = (exp_iz + exp_minus_iz) / (two);      // = cos(temp1)

                // store matricies in vector for later caching
                pos = mat_id*4 + dick_id*(4*num_mat) + stuetz_id*(4*num_mat*num_dick);

                precomp_matricies[ pos + 0  ] = temp3;
                precomp_matricies[ pos + 1  ] = temp2 / k_ix_omega_c0[(stuetz_id * num_mat) + mat_id];
                precomp_matricies[ pos + 2  ] = -(temp2 * k_ix_omega_c0[(stuetz_id * num_mat) + mat_id]);
                precomp_matricies[ pos + 3  ] = temp3;

            }
        }
    }
};


// only needed for testing, not part of the real computation any more
void calc_TransferMatrix(const complex<double> * __restrict__ k_ix_omega_c0 , const double * __restrict__ dn_thickness,
                         complex<double>  * __restrict__ tMatrix , complex<double>  * __restrict__ interim ,
                         const int num_schicht) {
    // allocate some memory
    complex<double> temp1;
    complex<double> temp2;
    complex<double> temp3;
    complex<double> exp_iz;
    complex<double> exp_minus_iz;

    // define constants
    complex<double> imag_unit{0,1};
    const std::complex<double> two {2, 0};
#pragma omp simd aligned( k_ix_omega_c0, dn_thickness : 64)  // bringt aber keinen Speedup
    for (int m_i = 1 ; m_i < num_schicht-1; ++m_i) {
        temp1 = k_ix_omega_c0[m_i] * dn_thickness[m_i] * imag_unit;
        exp_iz = exp(temp1);
        exp_minus_iz = exp(-temp1);
        temp2 = (exp_iz - exp_minus_iz) / (two * imag_unit);   // = sin(temp1)
        temp3 = (exp_iz + exp_minus_iz) / (two );      // = cos(temp1)

        // fill interim
        interim[0] = temp3;
        interim[1] = temp2 / k_ix_omega_c0[m_i];
        interim[2] = -(temp2 * k_ix_omega_c0[m_i]);
        interim[3] = temp3;

        // Matrix_product
        matrix_product(tMatrix , interim);
    }
}


void calc_TransferMatrix_precomp( std::complex<double> * __restrict__ precomp_matricies,
                                  std::complex<double>  * __restrict__ tMatrix,
                                  int *  __restrict__ rand_dick,
                                  const int num_mat,
                                  const int num_schicht,
                                  const int num_dick ,
                                  const int stuetz_id) {
    uint64_t pos;
    int mat_id;
#pragma omp simd aligned(rand_dick : 64)
    for (int m_i = 1 ; m_i < num_schicht-1; ++m_i) {

        mat_id = (m_i - 1) % num_mat ;

        pos = mat_id*4 + rand_dick[m_i]*(4*num_mat) + stuetz_id*(4*num_mat*num_dick);
        // matrix_product
        matrix_product(tMatrix , &precomp_matricies[pos]);
    }
}


// computes reflectivity with given transfer-matrix
double calc_RTP(const complex<double> * __restrict__ tMatrix, const double k_outx, const double k_inx ){
    complex <double> im_unit (0,1);  // imaginary unit
    complex<double> ZTE = k_inx * tMatrix[3] -  k_outx * tMatrix[0] - im_unit  * ( tMatrix[2] + k_inx * k_outx * tMatrix[1]);
    complex<double> NTE = k_outx * tMatrix[3] + k_inx * tMatrix[0] -  im_unit  * ( tMatrix[2] - k_inx * k_outx * tMatrix[1]);
    //nur Reflexion
    complex<double> RTE = ZTE / NTE;
    return pow(abs(RTE),2);
};


// checks if test_solution is a valid solution for our targets
bool test_solution(double * __restrict__ vec_targets , double * __restrict__ test_solution ,  int num_stuetz)
{
    int sum_bools = 0;
#pragma omp simd aligned( test_solution, vec_targets : 64)
    for (int i = 0; i < num_stuetz; i++){
        sum_bools += (test_solution[i] - vec_targets[i]) < 0 ;
    }
    if (sum_bools == num_stuetz) {
        return true;
    }
    return false;
}


void thin_film_calculator  (double *  __restrict__ wavelength,
                          double *  __restrict__ vec_brechzahl,
                          double *  __restrict__ vec_absorb,
                          double *  __restrict__ dn_thickness,
                          double *  __restrict__ vec_targets,
                          double *  __restrict__ final_solution,
                          int num_stuetz ,
                          int num_schicht,
                          int num_dick,
                          int num_mat,
                          int zwl,
                          int64_t max_num_iterations,
                          int size_L1_cache_in_KB = 256) {

    // #########################################################
    // part 1: build main data-struktures
    // #########################################################

    // convert wavelength
    aligned_vector<double> vec_omega(num_stuetz);
    double co = 2.0 * M_PI;
    for (int i = 0; i < num_stuetz; ++i) {
        vec_omega[i] = (co) / (wavelength[i]);
    }


    //Convert vec_ refractive index and vec_absorb into complex numbers and multiply by factor omega
    // k_ix_omega_c0 divide into vectors for the layers and vectors for the half spaces substrate (glass) and the medium (air)
    aligned_vector<complex<double>> k_ix_omega_c0_mat(num_mat * num_stuetz);
    aligned_vector<complex<double>> k_ix_omega_c0_air(num_stuetz);
    aligned_vector<complex<double>> k_ix_omega_c0_glas(num_stuetz);
    for (int i = 0; i < num_stuetz; ++i) {
        for (int mat_id = 0; mat_id < num_mat; mat_id++)
            k_ix_omega_c0_mat[i * num_mat + mat_id] = {vec_brechzahl[i * (num_mat + 2) + (mat_id + 1)] * vec_omega[i] ,
                                                       vec_absorb[i * (num_mat +2) + (mat_id + 1)] * vec_omega[i] };
    }

    for (int stuetz_id = 0; stuetz_id < num_stuetz; stuetz_id++) {
        k_ix_omega_c0_air[stuetz_id]  = {vec_brechzahl[stuetz_id * (num_mat + 2)] * vec_omega[stuetz_id], vec_absorb[stuetz_id * (num_mat + 2)] * vec_omega[stuetz_id]};
        k_ix_omega_c0_glas[stuetz_id] = {vec_brechzahl[stuetz_id * (num_mat + 2) + 3] * vec_omega[stuetz_id], vec_absorb[stuetz_id * (num_mat + 2) + 3] * vec_omega[stuetz_id]};
    }




    // datastructures for itermediate results
    aligned_vector<complex<double>> interim(4);
    aligned_vector<complex<double>> M{complex<double>(1, 0), complex<double>(0, 0), complex<double>(0, 0),
                                      complex<double>(1, 0)};
    double k_inx = 0;
    double k_outx = 0;
    aligned_vector<double> temp_refl_val(num_stuetz, 0);
    bool solution_found = false;


    // Precompute matrices
    aligned_vector<complex<double>> precomp_maticies(num_stuetz * num_dick * num_mat * 4);
    calc_interims(k_ix_omega_c0_mat.data(), dn_thickness, num_mat, num_stuetz, num_dick, precomp_maticies.data());


    // compute size of vector precomp_matricies in kbytes to fit it into L1-cache later
    uint64_t size_precomp_matricies_in_kbytes =  num_stuetz * num_dick * num_mat * 4 * 8 / 1000;



    // how many times we have to slice the data to fit into choosen cache
    int num_slices = size_precomp_matricies_in_kbytes / size_L1_cache_in_KB;

    // assert num_slices is at least 1
    if (0 == num_slices){
        num_slices = 1;
    }

    // only for testing
    //cout << "number of batches to compute: " << num_slices << endl;


    // #########################################################
    // Part 2: parallel ragion - the heavy computation
    // #########################################################
    //omp_set_num_threads(1);  // only for testing

   if (num_slices > 1){  // if data does not fit into targetted cache --> splitt it. This is equivalent to splitting the search space!!
       for (int t = 0; t < num_slices ; t++) {  // restart parallel region a few times to assure that search is evenly distributed over data slices
           if (1 == solution_found){
               break;
           }

           // we know limit the possible thickness values, so that the data fits into target cache
           std::vector<int> possible_rand_numbers(num_dick);
           // Fill it with numbers 0, 1, ..., max_num - 1)
           std::iota(possible_rand_numbers.begin(), possible_rand_numbers.end(), 0);
           std::random_device rd;
           std::mt19937 gen(rd());
           std::shuffle(possible_rand_numbers.begin(), possible_rand_numbers.end(), gen);


#pragma omp parallel default(none) shared(cout, solution_found, k_ix_omega_c0_air, k_ix_omega_c0_glas,  vec_omega, \
max_num_iterations, final_solution, num_schicht, num_stuetz, vec_targets, precomp_maticies, \
num_dick, num_mat, dn_thickness, num_slices, possible_rand_numbers) \
firstprivate(interim, M, k_inx, k_outx, temp_refl_val)
           {
               // initialize random engine for every thread
               int thread_id = omp_get_thread_num();
               std::random_device rd;
               std::mt19937 gen(rd() + thread_id); // every thread gets different random numbers
               // Mix all possible values

               std::uniform_int_distribution<> dis(0, num_dick/ num_slices);   // scale possible Random numbers to fit in cache
               aligned_vector <int> rand_dick(num_schicht,0);


#pragma omp for schedule (static)
               for (int64_t i = 0; i < max_num_iterations / num_slices; ++i) {
                   if (solution_found) // we found the solution, just continue iterating without doing anything
                       continue;

                   // fill random rand_dick
                   for (int m_i = 1 ; m_i < num_schicht-1; ++m_i) {
                       rand_dick[m_i] = possible_rand_numbers[dis(gen)];   // gives in every iteration different random numbers from a different sub-interval of the search space
                   }
                   for (int stuetz_id = 0; stuetz_id < num_stuetz; ++stuetz_id) {  // compute for every wavelength the reflactivity
                       M[0] = 1;
                       M[1] = 0;
                       M[2] = 0;
                       M[3] = 1;
                       calc_TransferMatrix_precomp(precomp_maticies.data(), M.data(), rand_dick.data(), num_mat, num_schicht, num_dick , stuetz_id);
                       k_inx =  k_ix_omega_c0_glas[stuetz_id].real();
                       k_outx = k_ix_omega_c0_air[stuetz_id].real();
                       temp_refl_val[stuetz_id] = calc_RTP(M.data(), k_outx, k_inx);

                   }
                   if (test_solution(vec_targets, temp_refl_val.data(), num_stuetz)) {
                       solution_found = true;
                       // only for testing
                       //cout << "Found a solution! " << endl;
                       //cout << "Solution found by thread: " << thread_id << "  in iteration: " << i - (thread_id * max_num_iterations) / omp_get_num_threads() << endl;
#pragma omp critical // for the extreme raw case that to threads find a solution at the same time
                       {
                           //cout << "Thickness values: " << endl;
                           for (int m = 1; m < num_schicht -1 ; m++) {
                               final_solution[m] = dn_thickness[rand_dick[m]];  // copy data to final soultion
                               //cout << final_solution[m] << endl;  // only for testing
                           }
                           //cout << "Reflection values: " << endl;
                           //for (auto iv : temp_refl_val) {
                           //    cout << iv << endl;  // only for testing
                           //}
                       }
                   }
               }
           }
       }
   } else {  // data already fits into target cache, nbo need to slice search space
#pragma omp parallel default(none) shared(cout, solution_found, k_ix_omega_c0_air, k_ix_omega_c0_glas,  vec_omega, \
max_num_iterations, final_solution, num_schicht, num_stuetz, vec_targets, precomp_maticies, \
num_dick, num_mat, dn_thickness) \
firstprivate(interim, M, k_inx, k_outx, temp_refl_val)
       {
           // initialize random engine for every thread
           int thread_id = omp_get_thread_num();
           std::random_device rd;
           std::mt19937 gen(rd() + thread_id);
           std::uniform_int_distribution<> dis(0, num_dick);
           aligned_vector <int> rand_dick(num_schicht,0);
#pragma omp for schedule (static)
           for (int64_t i = 0; i < max_num_iterations; ++i) {
               if (solution_found) // we found the solution, just continue iterating without doing anything
                   continue;
               // fill rand_dick
               for (int m_i = 1 ; m_i < num_schicht-1; ++m_i) {
                   rand_dick[m_i] = dis(gen);
               }
               for (int stuetz_id = 0; stuetz_id < num_stuetz; ++stuetz_id) {
                   M[0] = 1;
                   M[1] = 0;
                   M[2] = 0;
                   M[3] = 1;
                   calc_TransferMatrix_precomp(precomp_maticies.data(), M.data(), rand_dick.data(), num_mat, num_schicht, num_dick , stuetz_id);
                   k_inx =  k_ix_omega_c0_glas[stuetz_id].real();
                   k_outx = k_ix_omega_c0_air[stuetz_id].real();
                   temp_refl_val[stuetz_id] = calc_RTP(M.data(), k_outx, k_inx);

               }
               if (test_solution(vec_targets, temp_refl_val.data(), num_stuetz)) {
                   solution_found = true;
                   // only for testing
                   //cout << "Found a solution! " << endl;
                   //cout << "Solution found by thread: " << thread_id << "  in iteration: " << i - (thread_id * max_num_iterations) / omp_get_num_threads() << endl;
#pragma omp critical // for the extreme raw case that to threads find a solution at the same time
                   {
                       //cout << "Thickness values: " << endl;
                       for (int m = 1; m < num_schicht -1 ; m++) {
                           final_solution[m] = dn_thickness[rand_dick[m]];  // copy data to final soultion
                           //cout << final_solution[m] << endl; // only for testing
                       }
                       //cout << "Reflection values: " << endl;
                       //for (auto iv : temp_refl_val) {
                       //    cout << iv << endl; // only for testing
                       //}
                   }
               }
           }
       }
       // only for testing
       //if (!solution_found) {
       //    cout << "\n";
       //    cout << "No Solution found." << endl;
       //    cout << "\n";
       //}
   }
};
